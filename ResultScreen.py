from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QFont, QPalette, QColor
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton, QHBoxLayout
from Constants import Constants


class ResultScreen(QWidget):
	result_output = None
	clickSignal = pyqtSignal(str)
	def __init__(self, parent, result_output):
		super().__init__(parent)
		self.result_output = result_output
		self.mainLayout = QVBoxLayout()
		self.resize(Constants.MAIN_WINDOW_WIDTH, Constants.MAIN_WINDOW_HEIGHT)
		myPalette = self.palette()
		resultScreenBgColor = QColor(0, 0, 0)
		resultScreenBgColor.setAlpha(200)
		myPalette.setColor(QPalette.Background, resultScreenBgColor)
		self.setAutoFillBackground(True)
		self.setPalette(myPalette)
		self.setLayout(self.mainLayout)



		modalWidg = self.modalResult()
		self.mainLayout.addWidget(modalWidg)
		self.mainLayout.setAlignment(Qt.AlignCenter)

	def clickHandler(self, params):
		sender = self.sender()
		self.clickSignal.emit(sender.text())



	def modalResult(self):
		modalWidget = QWidget(self)
		modalLayout = QVBoxLayout()
		modalWidget.setLayout(modalLayout)

		modalLabel = QLabel(self.result_output, self)
		modalLabel.setAlignment(Qt.AlignCenter)
		font = QFont()
		font.setFamily("PT Sans")
		font.setPixelSize(26)
		modalLabel.setFont(font)

		modalButtonsWidget = QWidget(modalWidget)
		modalButtonsWidget.show()
		modalButtonsLayout = QHBoxLayout()
		modalButtonsWidget.setLayout(modalButtonsLayout)

		buttonAccept = QPushButton('Start new game', self)
		buttonCancel = QPushButton('Exit', self)
		buttonAccept.clicked.connect(self.clickHandler)
		buttonCancel.clicked.connect(self.clickHandler)
		modalButtonsLayout.addWidget(buttonAccept)
		modalButtonsLayout.addWidget(buttonCancel)
		modalLayout.addWidget(modalLabel)
		modalLayout.addWidget(modalButtonsWidget)

		modalPalette = modalWidget.palette()
		modalBgColor = QColor(255, 255, 255)
		modalPalette.setColor(QPalette.Background, modalBgColor)
		modalWidget.setAutoFillBackground(True)
		modalWidget.setPalette(modalPalette)
		modalWidget.show()
		return modalWidget


