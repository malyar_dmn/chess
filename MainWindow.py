import sys
from Board import Board
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import pyqtSlot
from StartScreen import StartScreen
from ResultScreen import *

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		self.setGeometry(300, 30, 1000, 1000)
		self.setWindowTitle('Chess')

		self.startGameWidget()
		self.generalLayout = QVBoxLayout()
		self.widget = QWidget(self)
		self.setCentralWidget(self.widget)
		self.widget.setLayout(self.generalLayout)

		self.generalLayout.addWidget(self.startScreen)
		self.generalLayout.setAlignment(Qt.AlignCenter)

	def startGameWidget(self):
		self.startScreen = StartScreen(self)
		self.startScreen.startGameButton.clicked.connect(self.startGame)

	def startGame(self):
		self.startScreen.close()
		self.board = Board(self)
		self.board.resultScreenSignal.connect(self.handleResultScreen)
		self.board.setMinimumSize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT)
		self.newGameButton = QPushButton('Start new game', self)

		font = QFont()
		font.setFamily("PT Sans")
		font.setPixelSize(26)
		self.newGameButton.setFont(font)

		self.generalLayout.addWidget(self.newGameButton)
		self.generalLayout.addWidget(self.board)
		self.generalLayout.setAlignment(Qt.AlignCenter)

		self.newGameButton.clicked.connect(self.handleNewGameButton)

	def resultGame(self, result_output):
		self.result = ResultScreen(self, result_output)
		self.result.clickSignal.connect(self.test)
		self.result.show()

	@pyqtSlot(str)
	def handleResultScreen(self, str):
		self.resultGame(str)

	def test(self, param):
		if param == 'Start new game':
			self.handleNewGameButton()
			self.result.close()
		elif param == 'Exit':
			self.result.close()
			self.close()
		else:
			return

	def handleNewGameButton(self):
		self.board.startNewGame()


if __name__ == "__main__":
	MyChess = QApplication(sys.argv)
	window = MainWindow()
	window.showFullScreen()
	window.showMaximized()
	sys.exit(MyChess.exec_())