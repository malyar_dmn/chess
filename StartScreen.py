from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton
from Constants import Constants


class StartScreen(QWidget):
	def __init__(self, parent):
		super().__init__(parent)

		self.resize(Constants.MAIN_WINDOW_WIDTH, Constants.MAIN_WINDOW_HEIGHT)
		self.font = QFont()
		self.font.setFamily("PT Sans")
		self.font.setPixelSize(26)
		self.setFont(self.font)

		self.boxHLayout = QVBoxLayout()
		self.setLayout(self.boxHLayout)
		self.backgroundImage()
		self.startGameButton = QPushButton('Start game', self)
		self.startGameButton.raise_()
		self.startGameButton.setStyleSheet('''padding: 10px''')

		self.boxHLayout.addWidget(self.startGameButton)

	def backgroundImage(self):
		label = QLabel(self)
		label.resize(600, 800)
		label.raise_()
		pix = QPixmap('assets/background.jpg')
		self.resize(pix.width(), pix.height())
		label.setPixmap(pix)
		self.boxHLayout.addWidget(label)




