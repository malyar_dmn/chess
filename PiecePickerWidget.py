import chess.svg

from PyQt5.QtWidgets import QHBoxLayout, QWidget
from PyQt5.QtCore import pyqtSlot

from BasicCell import *


class PiecePicker(QWidget):
	pickerSignal = pyqtSignal()
	def __init__(self, parent, color):
		super().__init__(parent)
		self.move(250, 50)
		self.generalLayout = QHBoxLayout()
		self.setLayout(self.generalLayout)
		self.pickedPiece = None
		self.renderPieceItems(color)


	def renderPieceItem(self, piece):
		pickItemCell = CellPickItem(self, piece)
		pickItemCell.raise_()
		pickItemCell.pickSignal.connect(self.onPickItem)
		pickItemCell.setStyleSheet('''background-color: #ccc''')

		self.generalLayout.addWidget(pickItemCell)

	def renderPieceItems(self, color):
		piecePickArr = [
			chess.Piece(chess.QUEEN, color),
			chess.Piece(chess.ROOK, color),
			chess.Piece(chess.KNIGHT, color),
			chess.Piece(chess.BISHOP, color)
		]

		for piece in piecePickArr:
			self.renderPieceItem(piece)


	@pyqtSlot()
	def onPickItem(self):
		item = self.sender()
		self.pickedPiece = item
		self.pickerSignal.emit()

